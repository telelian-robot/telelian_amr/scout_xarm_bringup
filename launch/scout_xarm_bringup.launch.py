import os
import launch
import launch_ros

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess, LogInfo
from launch_ros.substitutions import FindPackageShare
from launch.substitutions import FindExecutable, PathJoinSubstitution
from launch.substitutions import LaunchConfiguration, Command
from launch_ros.actions import Node

def generate_launch_description():
    robot_description_content = Command([
        PathJoinSubstitution([FindExecutable(name="xacro")]), " ",
        PathJoinSubstitution(
            [FindPackageShare("scout_xarm_description"), "urdf", "scout_xarm6.xacro"]
        ),
    ])
    rviz_config = get_package_share_directory('scout_xarm_bringup')+'/rviz/scout_xarm_with_lidar.rviz'

    scout_xarm_bringup_dir = get_package_share_directory('scout_xarm_bringup')
    scout_config_path = f'{scout_xarm_bringup_dir}/config/config.yaml'

    gripper_dir = get_package_share_directory('dh_gripper')
    gripper_config_path = f'{gripper_dir}/param/config.yaml'


    return LaunchDescription([
        DeclareLaunchArgument('use_sim_time', default_value='false',
            description='Use simulation clock if true'),

        LogInfo(msg='use_sim_time: '),
        LogInfo(msg=LaunchConfiguration('use_sim_time')),
        
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            parameters=[{
                'use_sim_time': LaunchConfiguration('use_sim_time'),
                'robot_description':robot_description_content
            }]),
        Node(
            package='joint_state_publisher',
            executable='joint_state_publisher',
            name='joint_state_publisher',
            output='screen',
            parameters=[{
                'use_sim_time': LaunchConfiguration('use_sim_time'),
                'robot_description':robot_description_content
            }]),
        Node(
            namespace='rslidar_sdk',
            package='rslidar_sdk', 
            executable='rslidar_sdk_node',
            output='screen'),
        
        Node(
            namespace='rviz2',
            package='rviz2',
            executable='rviz2',
            arguments=['-d',rviz_config]),
        Node(
            package='scout_base',
            executable='scout_base_node',
            output='screen',
            emulate_tty=True,
            parameters=[scout_config_path],
            # arguments=['--ros-args', '--log-level', LaunchConfiguration('log-level')],
        ),
        Node(
            package='dh_gripper',
            executable='sub',
            name='sub',
            parameters=[gripper_config_path],
            output='screen'),
    ])
